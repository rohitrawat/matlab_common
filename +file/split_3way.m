function [trgfname, valfname, tstfname] = split_3way(fname, N, M, isClassifier, ratios, precision_string)
% [trgfname, valfname, tstfname] = split_3way(fname, N, M, isClassifier, ratios, precision_string)
% This program splits a data file into three randomly sampled exclusive
% parts.
% ratios is vector with 3 elements which specifies the ratio of data in the
% three output files. E.g. ratios = [0.6 0.2 0.2] or ratios = [0.33 0.33
% 0.33].
% It returns the file names of the three files produced.
% Author: Rohit Rawat (rohitrawat@gmail.com)


if(nargin<5)
    ratios = [0.7 0.15 0.15];
else
    if(round(sum(ratios)*10)~=10)
        error('Ratios must add up to 1 and must have at least 2 digit accuracy after the decimal.');
    end
end

if(nargin<6)
    precision_string = '%e';
end

% basename = utils.getfoldername(fname);
basename = fname;
trgfname = [basename '.train'];
valfname = [basename '.validate'];
tstfname = [basename '.test'];
if(isClassifier)
    M1 = 1;
    [x t Nv] = file.read_class_file(fname, N, M1);
else
    [x t Nv] = file.read_approx_file(fname, N, M);
end

idx=randperm(Nv);
Nv_train = round(Nv*ratios(1))
Nv_val = min(Nv - Nv_train, round(Nv*ratios(2)))
Nv_test = Nv - Nv_train - Nv_val
x_train = x(idx(1:Nv_train),:);
t_train = t(idx(1:Nv_train),:);
x_val = x(idx(Nv_train+1:Nv_train+Nv_val),:);
t_val = t(idx(Nv_train+1:Nv_train+Nv_val),:);
x_test = x(idx(Nv_train+Nv_val+1:end),:);
t_test = t(idx(Nv_train+Nv_val+1:end),:);
dlmwrite(trgfname, [x_train t_train], 'delimiter', '\t', 'precision', precision_string);
if(Nv_val>0)
    dlmwrite(valfname, [x_val t_val], 'delimiter', '\t', 'precision', precision_string);
end
if(Nv_test>0)
    dlmwrite(tstfname, [x_test t_test], 'delimiter', '\t', 'precision', precision_string);
end
if(Nv ~= size(x_train,1)+size(x_val,1)+size(x_test,1))
    error('Error splitting correctly.');
end
