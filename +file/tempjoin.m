function newfname = tempjoin(fnames, N, M, ftype, newfname)
% Function to join two files into a temporary.
% Rohit Rawat (rohitrawat@gmail.com)

precision_string = '%e';

x1 = [];
t1 = [];
for i=1:length(fnames)
    if(ftype == 2)
        M1 = 1;
        [x t Nv] = file.read_class_file(fnames{i}, N, M1);
    elseif ( ftype == 1)
        [x t Nv] = file.read_approx_file(fnames{i}, N, M);
    end
    x1 = [x1; x];
    t1 = [t1; t];
end

dlmwrite(newfname, [x1 t1], 'delimiter', '\t', 'precision', precision_string);
