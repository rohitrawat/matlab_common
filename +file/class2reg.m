function [regfname, M] = class2reg(fname, N, coded)

if(nargin<3)
    coded = false;
end

if(nargin<6)
    precision_string = '%e';
end

[x, ic, Nv] = file.read_class_file(fname, N, 1);

Nc = max(ic)

if(coded)
    
    M = length(dec2bin(Nc))

    t = zeros(Nv,M);
    u=dec2bin(ic,M);

    for j=1:M
        t(:,j) = str2num(u(:,j));
    end

else
    
    M = Nc;
    t = zeros(Nv,M);
    for p = 1:Nv
        t(p,ic(p)) = 1;
    end
    
end    

[a b c] = fileparts(fname);
regfname = fullfile([b '_reg' c]);
disp(['Writing to file ' regfname]);
dlmwrite(regfname, [x t], 'delimiter', '\t', 'precision', precision_string);
