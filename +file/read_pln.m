function pln = read_pln(fname)

fp = fopen(fname, 'r');
if(fp==-1)
    error('Unable to open file to load PLN.\n');
end

fscanf(fp, 'PLN\n');

Nc = fscanf(fp, 'K\n%d\n');

N = fscanf(fp, 'N\n%d\n');

M = fscanf(fp, 'M\n%d\n');

myfprintf('N=%d, M=%d, Nc=%d\n', N, M, Nc);

fscanf(fp, 'DistanceMeasure\n');
myfprintf('DistanceMeasure\n');
distanceMeasure = fscanf(fp, '%lf ')';
myfprintf('%e ', distanceMeasure);
fscanf(fp, '\n');
myfprintf('\n');

fscanf(fp, 'InputMeans\n');
myfprintf('InputMeans\n');
inputMeans = fscanf(fp, '%lf ')';
myfprintf('%e ', inputMeans);
fscanf(fp, '\n');
myfprintf('\n');

fscanf(fp, 'InputStd\n');
myfprintf('InputStd\n');
inputStd = fscanf(fp, '%lf ')';
myfprintf('%e ', inputStd);
fscanf(fp, '\n');
myfprintf('\n');

clusterCenters = zeros(Nc,N);
R = cell(Nc,1);
C = cell(Nc,1);
W = cell(Nc,1);
Et = cell(Nc,1);

for k = 1:Nc
    idx = fscanf(fp, 'ClusterIndex\n%d\n');
    if(k~=(idx+1))
        error('Index mismatch!');
    end
    myfprintf('%d\n', idx);

    Nv(k) = fscanf(fp, 'Nv\n%d\n');
    
    myfprintf('Nv\n%d\n', Nv(k));

    fscanf(fp, 'CenterVector\n');
    clusterCenters(k,:) = fscanf(fp, '%lf ');
    fscanf(fp, '\n');
    
    myfprintf('CenterVector\n');
    myfprintf('%e ', clusterCenters(k,:));
    myfprintf('\n');
% TODO save R/W etc in cell array
    fscanf(fp, 'R\n');
    Nu = N+1;
    R{k} = reshape(fscanf(fp, '%lf '), [Nu Nu])';

%     myfprintf('R\n');
%     R(1:2,1:5)
    
    fscanf(fp, 'C\n');
    C{k} = reshape(fscanf(fp, '%lf '), [Nu M])';
    
%     myfprintf('C\n');
%     C(1:2,1:5)

    fscanf(fp, 'W\n');
    W{k} = reshape(fscanf(fp, '%lf '), [Nu M])';

%     myfprintf('W\n');
%     W(1:2,1:5)

    fscanf(fp, 'Et\n');
    Et{k} = fscanf(fp, '%lf ')';

%     myfprintf('Et\n');
%     Et

    lambda(k) = fscanf(fp, 'lambda\n%lf\n');

%     myfprintf('lambda\n');
%     lambda(k)

end

fscanf(fp, 'EOF\n');

fclose(fp);

pln = struct;
pln.cluster_centers = clusterCenters;
pln.cluster_R = R;
pln.cluster_C = C;
pln.cluster_Et = Et;
pln.cluster_W = W;
pln.cluster_lambda = lambda;
pln.Nc = Nc;
pln.N = N;
pln.M = M;
pln.input_means = inputMeans;
pln.input_std = inputStd;
pln.distance_measure = distanceMeasure;
pln.cluster_Nv = Nv;
pln.cluster_lambda = lambda;

function myfprintf(varargin)
%  fprintf(varargin)
