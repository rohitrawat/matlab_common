function [trg_names val_names] = split_3way_trg_val(fname, N, M, ftype, parts, basename, doNormalize)
% Function to split a file into multiple training and validation set pairs.
% 1. File is split into *parts* equal parts.
% 2. Each part is saved as a validation set and the remaining are saved as the training set.
% 3. The file names are returned.
% The files are stored in the same location as the input file.
% Rohit Rawat (rohitrawat@gmail.com)

if(nargin<6)
    basename = fname;
end

if(nargin<7)
    doNormalize = false;
end

ratios = ones(1,parts)/parts;

precision_string = '%e';

if(ftype == 2)
    M1 = 1;
    [x t Nv] = file.read_class_file(fname, N, M1);
elseif ( ftype == 1)
    [x t Nv] = file.read_approx_file(fname, N, M);
end

if(doNormalize)
    m = mean(x,1);
    s = std(x, 1, 1);
    x = bsxfun(@minus, x, m);
    x = bsxfun(@rdivide, x, s);
end

Nv_all = round(double(Nv)*ratios);
Nv_all(end) = Nv - sum(Nv_all(1:end-1));

idx=randperm(Nv);

start = 0;
for i=1:parts
    idxidx = start+1:start+Nv_all(i);
    start = start+Nv_all(i);
    x_parts{i} = x(idx(idxidx),:);
    t_parts{i} = t(idx(idxidx),:);
end

for i=1:parts
    x_val = x_parts{i};
    t_val = t_parts{i};
    val_names{i} = [basename '-val-set-' num2str(i)];
    dlmwrite(val_names{i}, [x_val t_val], 'delimiter', '\t', 'precision', precision_string);
    
    x_tra = [];
    t_tra = [];
    for j=setdiff(1:parts,i)
        x_tra = [x_tra; x_parts{j}];
        t_tra = [t_tra; t_parts{j}];
    end
    trg_names{i} = [basename '-tra-set-' num2str(i)];
    dlmwrite(trg_names{i}, [x_tra t_tra], 'delimiter', '\t', 'precision', precision_string);
end

