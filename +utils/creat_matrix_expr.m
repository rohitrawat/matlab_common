function creat_matrix_expr(M)

fprintf('[ ');
for i=1:size(M,1)
    for j=1:size(M,2)
        fprintf('%f, ', M(i,j));
    end
    fprintf('\b\b;\n');
end
fprintf('\b\b ]');

