function [optimized_ic Nc mapping] = optimize_classes(ic)
% takes a vector of class numbers and returns newly assigned class numbers
% which start at 1 and have no gaps in optimized_ic.
% mapping is a vecotr which gives the original ids from the new class id

[icu, iic, iicu] = unique(ic);
Nc = length(icu);
optimized_ic = iicu;
mapping = icu;
