function p = tempdir2()
% returns a temporary directory in the RAMDRIVE for my machine

p = tempdir;

if(ispc)
    if(length(getenv('WINEPREFIX'))==0) 
        % XPS machine
        if(exist('I:\TEMP\', 'dir'))
            p = 'I:\TEMP\';
        end
    else
        % Under WINE
        if(exist('Z:\home\rohit\tmpfs\', 'dir'))
            p = 'Z:\home\rohit\tmpfs\';
        elseif(exist('C:\users\rohit\tmpfs\', 'dir'))
            p = 'C:\users\rohit\tmpfs\';
        end
    end
else
    % LINUX
    if(exist('/home/rohit/tmpfs/', 'dir'))
        p = '/home/rohit/tmpfs/';
    end
end
