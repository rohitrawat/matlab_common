function path2 = fix_path(path)
% converts windows paths to Unix paths, provided they have a common root
% fodler along the path.

% detect OS
if(isunix)
    path2 = strrep(path, '\', filesep);
elseif(ispc)
    path2 = strrep(path, '/', filesep);
    
    %check for absolute paths
    if( (path2(1) == filesep) || (isalnum(path2(1)) && path2(2)==':') )
        disp('Detected absolute path');
        
        if(length(getenv('WINEPREFIX')>0) || path(1) == '/')
            disp('Detected wine..');
            % converting an absolute unix path to wine
            % there will be at lease one folder (home?) in the absolute path
            % which will be a part of the current path
            while(1)
                [key remain] = strtok(path2, '\');
                p = pwd;
                idx=strfind(p, key);
                if(~isempty(idx))
                    path2 = fullfile(p(1:idx-1), path2);
                    return;
                else
                    if(isempty(remain))
                        disp('failed');
                        break;
                    end
                    path2 = remain;
                end
            end
        end
    end
    
end

function y = isalnum( x )

y = all( (x>='0' & x<='9') | (x>='a' & x<='z') | (x>='A' & x<='Z') );
