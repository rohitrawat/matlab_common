function seed_rand(seed)
% Seeds random number generator. it works for both new and old versions of
% MATLAB.
% Usage:
%   seed_rand(seed)
% Rohit Rawat, 2014

if(exist('rng'))
    rng(seed);
elseif(exist('RandStream'))
    s=RandStream('mt19937ar','Seed', seed); RandStream.setDefaultStream(s);
else
    error('Could not re-seed the random number generator!');
end
