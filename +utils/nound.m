function out = nound(in)
% NO UNDerline. Usage:
%    out = nound(in)
% ecapes underscores in the input string(s)
% input can be a single string or a cell array of strings
% useful for fixing strings before using them in figure titles.

if(iscell(in))
    for i=1:length(in)
        out{i} = fix(in{i});
    end
else
    out = fix(in);
end

function out = fix(in)
out = strrep(in, '_', '\_');
