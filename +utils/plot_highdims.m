function plot_highdims(x, u)
% Plots high dimensional data in two dimensions. Plots certain data points
% with special markers.
% x: specifies all the data
% u: specail data points to be highlighted. can be [].

[X mu sigma] = zscore(x);  % zscore normalizes data cols

U = bsxfun(@rdivide, bsxfun(@minus, u, mu), sigma);
X1 = [X; U];
d = pdist(X1);
y = mdscale(d,2);
% glyphplot(X1, 'glyph', 'star', 'centers', y)
plot(y(1:size(X, 1), 1), y(1:size(X, 1), 2), '.r');
hold on;
plot(y(size(X, 1)+1:end, 1), y(size(X, 1)+1:end, 2), 'x', 'MarkerSize', 12, 'LineWidth', 3);
hold off;
