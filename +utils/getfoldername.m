function foldername = getfoldername(path)
% foldername = getfoldername(path) parses out the folder name from a given
% path.

foldername = '';
if(isempty(path))
    return;
end
if(path(end)==filesep)
    path = path(1:end-1);
end
[a foldername c] = fileparts(path);
foldername = lower(foldername);
