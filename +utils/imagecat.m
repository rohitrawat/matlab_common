function image = imagecat(top, bottom, gap)
% concatenates two images. gap is the desired separation in pixels.
%   image = imagecat(top, bottom, gap)

if(nargin < 3)
    gap = 0;
end
gapcolor = 128;

W = max(size(top,2), size(bottom,2));
H = sum(size(top,1), size(bottom,1));

d1 = W -size(top,2);
d2 = W -size(bottom,2);

top = padarray(top, [0 d1], 0, 'post');
bottom = padarray(bottom, [0 d2], 0, 'post');
image = [top; ones(gap,W)*gapcolor; bottom];
