function qlog(varargin)
% This is a quick logging and printing function. It can be used to replace
% all disp()/fprintf() calls in your code and the messages will be
% displayed to the screen as well as written to a log file (log.txt). The
% file is always appended to.
%  qlog() : creates/appends to the file log.txt and prints the current time
%  stamp and a separator to it.
%  qlog('fmt', args...) : this is a printf() replica that prints to the
%  screen and also writes to the log file. Usage is just like printf().
% 
% Author: Rohit Rawat (rohitrawat@gmail.com)
% Created: April 1, 2014

persistent fid;

if(nargin==0 || isempty(fid) || fid==-1)
    fid = fopen('log.txt', 'a');
    if(fid==-1)
        fprintf(2, 'Error opening log file!\n');
        fprintf(1, varargin{1}, varargin{2:end});
        return;
    end
    temp = sprintf('\n\nLogging started on %s\n', datestr(now));
    tee(fid, temp);
    tee(fid, '%c', ones(1,length(temp)-2)*'=');
    tee(fid, '\n\n');
    if(nargin==0)
        return;
    end
end

% write to file and screen
tee(fid, varargin{:});

end

function tee(fid, varargin)

% print to file
fprintf(fid, varargin{1}, varargin{2:end});
% print to stdout
fprintf(1, varargin{1}, varargin{2:end});

end
