function ret = load_mlp_path()

persistent current_path

if(isempty(current_path))
    current_path = path();
end
ret = current_path;

addpath(utils.fix_path('C:\Users\Rohit\Dropbox\MATLAB\hwo_molf_pruning'));
