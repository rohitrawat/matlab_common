function index = find_str_cells(string, cells)
% Usage: index = find_str_cells(string, cells)
% returns -1 if not found, else returns index starting at 1.

for i=1:length(cells)
    if(strcmpi(string, cells{i}))
        index = i;
        return;
    end
end

index = -1;
return;
