function prefix = dateprefix(output_path)
% prefix = dateprefix(output_path)
% if no arguments or [] is specified, simply return a date prefix mm-dd-yyyy_HH-MM_
% if a path is specified, returns path/mm-dd-yyyy_HH-MM_

if(nargin<1)
    output_path = [];
end

prefix = datestr(now, 'mm-dd-yyyy_HH-MM_');
if(nargin==1)
    prefix = fullfile(output_path, datestr(now, 'mm-dd-yyyy_HH-MM_'));
end
