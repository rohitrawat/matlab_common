MATLAB common use functions
---------------------------

Utils package
=============

dateprefix.m  : creates a prefix like 'YYYY_MM_DD_HH_MM_'
fix_path.m  : converts Windows paths to Linux asuming the 'Dropbox' folder as the common link
getfoldername.m  : extracts the folder name from a path (basename)
imagecat.m  : concatenates images by tiling them vertically
seed_rand.m  : seeds MATLAB random number generator
tempdir2.m  : fetches a temp directory address from a custom ramdrive location

File package
============

read_approx_file.m  : Reads approximation data file.
read_class_file.m  :Reads classification data file.

